#!/bin/bash
if [ ! "$1" ]; then
    echo "Usage: $0 IP"
    exit;
fi;

iptables -A TRAFFIC_IN -s "$1" -p tcp
iptables -A TRAFFIC_IN -s "$1" -p udp

iptables -A TRAFFIC_OUT -s "$1" -p tcp
iptables -A TRAFFIC_OUT -s "$1" -p udp
