<div class="row">
    <div class="two-half column">
        <h4>Profil for {$name}</h4>
        {if isset($error)}<p class="error">{$error}</p>{/if}
        {$form}
        <p><a href="?p=profil">Back to the profile...</a></p>
    </div>
</div>
