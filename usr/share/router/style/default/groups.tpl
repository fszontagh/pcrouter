<div class="row">
    <div class="nine columns">
        {if isset($error)}<p class="error">{$error}</p>{/if}


        <h4>Groups</h4>
        <table class="u-full-width">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Group ID</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {loop $groups}
                {if $injob==1 }
                    <tr class="disabled">
                    {else}
                    <tr>
                    {/if}
                    <td class="delete_title">{$name}</td>
                    <td>{$guid}</td>
                    {if $guid >1000 AND $injob == 0}
                        <td><a class="delete" href="?p=groups&delete={$id}"></a></td>
                        {else}
                        <td>&nbsp;</td>
                    {/if}
                </tr>
                {/loop}
            </tbody>
        </table>
        {if isset($pager)}
            {$pager}
        {/if}
    </div>
    <div class="three columns">
        <h5>Commands</h5>
        <ul>
            <li><a href="?p=group_add">Add group</a></li>
        </ul>
    </div>
</div>