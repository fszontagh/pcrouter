<?php

$q = $db->query("SELECT * FROM groups ORDER BY guid DESC");

$groups = array();

while ($group = $q->fetch_assoc()) {
    $groups[] = $group;
}

if (isset($_GET["delete"]) AND is_numeric($_GET["delete"])) {
    $id = intval($_GET["delete"]);
    //query group info
    $delq = $db->query("SELECT id,name,guid FROM groups WHERE id = " . $id);
    $grp = $delq->fetch_assoc();

    $cmd = "/usr/sbin/groupdel \"" . $grp["name"] . "\"";
    $params["group_name"] = $grp["name"];
    $params["guid"] = $grp["guid"];
    $params["id"] = $grp["id"];
    if ($queue->add("group_del", $user->id, $cmd, $params)) {
        $db->query("update `groups` set injob = 1 WHERE id = " . $id);
    }

    header("Location: ?p=groups");
    exit;

    /*
      $delq = $db->query("SELECT name FROM groups WHERE id = ".$del);
      $grp = $delq->fetch_assoc();
      exec("/usr/sbin/groupdel ".$grp["name"]);
      $db->query("delete from groups where id = ".$del); */
    /*  if (!empty($db->error)) {
      $tpl->add("error",$db->error);
      $tpl->show("error.tpl");
      }else{
      syslog(LOG_INFO,"Deleted new group: ".$grp["name"]." by: ".$user->username());
      header("Location: ?p=groups");
      exit;
      } */
}


$tpl->add("groups", $groups);
$tpl->show("groups.tpl");
