<?php

//header("Content-Type: application/json; charset=utf-8");
$old_error_handler = set_error_handler("syserrorhandler");
$CONFIG = include '/etc/router/config.php';
include_once('libs/user.class.php');
$user = new user($CONFIG["SYSTEM_KEY"], $CONFIG);

if ($user->is_authed == false) {
    json_encode(array("error" => "You are not logged in!"));
    exit;
}

if (isset($_GET["mac"]) AND ! empty($_GET["mac"])) {
    $client = htmlspecialchars($_GET["mac"]);
    $db = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);
    $stats = array();
    $query = $db->query("SELECT `in`,`out`, `date` FROM stats WHERE mac = '" . $client . "' ORDER BY date DESC LIMIT 0,12");
    if (isset($db->error) AND ! empty($db->error)) {
        echo $db->error;
        exit;
    }
    $counter = 0;
    while ($stat = $query->fetch_assoc()) {
        $stats["DataSetsIn"][] = $stat["in"];
        $stats["DataSetsOut"][] = $stat["out"];
        $label = strtotime($stat["date"].'00');        
        $stats["DataLabels"][] = date("Y-m-d H:i",$label);
        $counter++;
    }
    krsort($stats["DataSetsIn"]);
    krsort($stats["DataSetsOut"]);
    krsort($stats["DataLabels"]);
    
    foreach ($stats["DataSetsIn"] as $k=>$v) {
        $tmp[] = $v;
    }
    $stats["DataSetsIn"] = $tmp;
    unset($tmp);
    foreach ($stats["DataSetsOut"] as $k=>$v) {
        $tmp[] = $v;
    }
    $stats["DataSetsOut"] = $tmp;
    unset($tmp);
    foreach ($stats["DataLabels"] as $k=>$v) {
        $tmp[] = $v;
    }
    $stats["DataLabels"] = $tmp;
    
    exit(json_encode($stats));
}

function syserrorhandler($errno, $errstr, $errfile, $errline) {
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting
        return;
    }

    switch ($errno) {
        case E_USER_ERROR:
            syslog(LOG_ERR, $errfile . " (" . $errline . "): " . $errstr);
            break;

        case E_USER_WARNING:
            syslog(LOG_WARNING, $errfile . " (" . $errline . "): " . $errstr);
            break;

        case E_USER_NOTICE:
            syslog(LOG_NOTICE, $errfile . " (" . $errline . "): " . $errstr);
            break;

        default:
            syslog(LOG_WARNING, $errfile . " (" . $errline . "): " . $errstr);
            break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}
