<?php

$countq = $db->query("select count(id) as c from users");
$count = $countq->fetch_assoc();

$pagination = new pagination($count["c"],10);

$userq = $db->query("SELECT users.*, clients.id as cid FROM users, clients WHERE clients.ip = users.last_ip ORDER BY id DESC ".$pagination->limit());


$users = array();
while ($usr = $userq->fetch_assoc()) {
    $usr["last_login"] = date("Y-m-d H:i",$usr["last_login"]);
    $users[] = $usr;
}

$tpl->add("users",$users);
$tpl->add("pager",$pagination->display());

$tpl->show("users.tpl");