<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $title;  ?></title>
  <meta name="description" content="">
  <meta name="author" content="">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="style/default/js/chart.js"></script>
  <script type="text/javascript" src="style/default/js/own.js"></script>
  <script src="ckeditor/ckeditor.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="style/default/css/normalize.css">
  <link rel="stylesheet" href="style/default/css/skeleton.css">
  <link rel="stylesheet" href="style/default/css/own.css">
  <link rel="icon" type="image/png" href="style/default/images/favicon.png">
</head>
<body>
  <div class="container">
