<?php

$url = "?p=dashboard";
$deleted = 0;
$where=" WHERE deleted = 0 ";
if (isset($_GET["deleted"])) {
    if ($_GET["deleted"] == 0) {
        $deleted = 0;        
    } else {
        $where="";
        $deleted = 1;
    }
}




$clients = $db->query("SELECT * FROM clients " . $where . " ORDER BY date ASC");
$c = array();
while ($client = $clients->fetch_assoc()) {
    $client["date"] = date("Y-m-d H:i:s",$client["date"]);    
    $c[] = $client;
}


$tpl->add(array("clients" => $c,
    "name" => $user->name(),
    "url" => $url,
    "deleted"=>$deleted
        ));

$tpl->show("dashboard.tpl");

