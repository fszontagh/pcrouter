<?php

$f = new form2("add_group");
$name = $f->addInput("text", "group_name", "Name:");
$f->addButton("save", "Save");


$name->addAttr("required", true);
$name->addAttr("min-length", 2);
$name->addAttr("max-length", 64);

if ($f->validate() === true) {
    $cmd = "/usr/sbin/groupadd \"" . $name->value() . "\"";
    $params["group_name"] = $name->value();
    $queue->add("group_add", $user->id, $cmd, $params);
    header("Location: ?p=groups");
    exit;
}


$tpl->add("form", $f->show());
$tpl->show("group_add.tpl");
