<?php

$id = 0;

if (isset($_GET["id"]) AND is_numeric($_GET["id"])) {
    $id = intval($_GET["id"]);
    $query = $db->query("SELECT * FROM clients WHERE id = " . $id);
    $client = $query->fetch_assoc();
    $client["fixed"] = false;
    $normal_mac = str_replace(":", "_", $client["mac"]);
    $fixed_file = $CONFIG["dnsmasq_confdir"] . DIRECTORY_SEPARATOR . $normal_mac;


    if (file_exists($fixed_file)) {
        $client["fixed"] = true;
    }


    ///fix the ip to the client
    if (isset($_GET["fixip"]) AND $client["fixed"] === false) {
        $pattern = array(":mac", ":ip", ":host");
        $target = array($client["mac"], $client["ip"], $client["host"]);
        $string = str_replace($pattern, $target, $CONFIG["dnsmasq_fixip_string"]);
        file_put_contents($fixed_file, $string);
        exec("service dnsmasq restart");
        header("Location: /?p=client&id=" . $id);
        exit;
    }

    //remove fixed IP
    if (isset($_GET["remove_fixip"]) AND $client["fixed"] === true) {
        unlink($fixed_file);
        exec("service dnsmasq restart");
        header("Location: /?p=client&id=" . $id);
        exit;
    }
    //change fixed IP
    if (isset($_GET["change_ip"]) AND $client["fixed"] === true) {
        $content = file_get_contents($fixed_file);
        $form = new form2();
        $ipf = $form->addInput("text", "ip", "IP", $client["ip"]);
        $ipf->addAttr("title", "Original IP: " . $client["ip"]);
        $ipf->addAttr("required", true);
        $form->addButton("save", "Save");
        $tpl->add("form", $form->show());
        if ($form->validate() === true) {
            $pattern = array(":mac", ":ip", ":host");
            $target = array($client["mac"], $ipf->value, $client["host"]);
            $string = str_replace($pattern, $target, $CONFIG["dnsmasq_fixip_string"]);
            file_put_contents($fixed_file, $string);
            exec("service dnsmasq restart");
            header("Location: /?p=client&id=" . $id);
            exit;
        }
    }

    $tpl->add($client);
    //query statistics from iptables
    include_once "libs/phpMyGraph.php";
    $in_stat = "/var/www/html/router/statistics/" . $client["host"] . "_in.png";
    $out_stat = "/var/www/html/router/statistics/" . $client["host"] . "_out.png";
    $statq = $db->query("select * from stats where mac = '" . $client["mac"] . "' ORDER BY date DESC LIMIT 12 ");
    if ($db->error) {
        syslog(E_ERROR, $db->error);
    }
    $data_in = array();
    $data_out = array();
    while ($stat = $statq->fetch_assoc()) {
        $data_in[$stat["date"]] = $stat["in"];
        $data_out[$stat["date"]] = $stat["out"];
    }

    foreach ($data_in as $k => $v) {
        $t = strtotime($k . "00");
        $datain[date("H:00", $t)] = $v;
    }


    foreach ($data_out as $k => $v) {
        $t = strtotime($k . "00");
        $dataout[date("H:00", $t)] = $v;
    }
    unset($data_in);
    unset($data_out);

    if (file_exists($in_stat)) {
        unlink($in_stat);
    }
    if (file_exists($out_stat)) {
        unlink($out_stat);
    }
    $graph_in = new phpMyGraph();
    $graph_in->parseVerticalSimpleColumnGraph($datain, array("title" => $client["host"] . " - Inbound traffic (in last 12h)", "file-name" => $in_stat, "transparent-background" => true));
    $graph_out = new phpMyGraph();
    $graph_out->parseVerticalSimpleColumnGraph($dataout, array("label" => "Hour", "title" => $client["host"] . " - Outbund traffic (in last 12h)", "file-name" => $out_stat, "transparent-background" => true));
} else {
    $tpl->add("error", "Client not found!");
}
$tpl->show("client.tpl");
