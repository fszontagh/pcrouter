$(document).ready(function(){
    $(".delete").click(function(){
        var title = $($(".delete_title")[$(this).index()]).text();
        if (title) {
            var accept_ = confirm("Do you really want to delete '"+title+"'?");
            return accept_;
        }
        return true;
    });
});