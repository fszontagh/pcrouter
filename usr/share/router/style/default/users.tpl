<div class="row">
    <div class="nine columns">
        {if isset($error)}<p class="error">{$error}</p>{/if}


        <h4>Users</h4>
        <table class="u-full-width">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Real Name</th>
                    <th>E-Mail</th>
                    <th>Last Login</th>
                    <th>Last Ip</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {loop $users}
                <tr>
                    <td><a href="?p=user&id={$id}">{$username}</a></td>
                    <td>{$realname}</td>
                    <td>{$email}</td>
                    <td>{$last_login}</td>
                    <td><a href="?p=client&id={$cid}">{$last_ip}</a></td>
                    <td><a href="?p=user_edit&id={$id}">EDIT</a></td>
                </tr>
                {/loop}
            </tbody>
        </table>
        {if isset($pager)}
            {$pager}
        {/if}
    </div>
    <div class="three columns">
        <h5>Commands</h5>
        <ul>
            <li>
                <a href="?p=user_add">Add user</a>
            </li>
        </ul>
    </div>

</div>