<div class="row">
      <div class="nine columns">
        <h4>Hello {$name}!</h4>        
        {if isset($error)}<p class="error">{$error}</p>{/if}


        <h6>Active PCs</h6>
<table class="u-full-width">
  <thead>
    <tr>
      <th>Host</th>
      <th>IP</th>
      <th>MAC</th>
      <th>Interface</th>
      <th>Updated</th>
    </tr>
  </thead>
  <tbody>
      {loop $clients}
    <tr {if $deleted==1}class="red_bcg"{/if}>
      <td>{$host}</td>
      <td><a href="?p=client&id={$id}">{$ip}</a></td>
      <td>{$mac}</td>
      <td>{$iface}</td>
      <td>{$date}</td>
    </tr>
    {/loop}
  </tbody>
</table>        
        

     
      </div>
        <div class="three columns">   
            <h4>FILTER</h4>
            <ul>
                {if $deleted == 1}
                <li><a href="{$url}&deleted=0">Show only active</a></li>
                {else}
                <li><a href="{$url}&deleted=1">Show all</a></li>
                {/if}
            </ul>
        <h5>Naviagtion</h5>
        <ul>
            <li>
                <a href="?p=dashboard">Client's list</a>
            </li>
            <li>
                <a href="?p=users">Users</a>
            </li>
        </ul>            
        </div>
      
      
    </div>

