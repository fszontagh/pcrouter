#/bin/bash

echo "Installing dependencies...:"

apt-get update
apt-get install ldap-auth-client nscd -y

auth-client-config -t nss -p lac_ldap

AUTOHOME="Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
        required                        pam_mkhomedir.so umask=0022 skel=/etc/skel"
        
echo "$AUTOHOME" > /usr/share/pam-configs/my_mkhomedir


#restart nscd
/etc/init.d/nscd restart

echo "LDAP Auth configured, please verify it: id someLdapUsername"