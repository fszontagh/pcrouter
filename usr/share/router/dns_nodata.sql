-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `access_log`;
CREATE TABLE `access_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_since_epoch` decimal(15,3) DEFAULT NULL,
  `time_response` int(11) DEFAULT NULL,
  `ip_client` char(15) CHARACTER SET latin1 DEFAULT NULL,
  `ip_server` char(15) CHARACTER SET latin1 DEFAULT NULL,
  `http_status_code` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `http_reply_size` int(11) DEFAULT NULL,
  `http_method` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `http_url` text CHARACTER SET latin1,
  `http_username` varchar(20) DEFAULT NULL,
  `http_mime_type` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `squid_hier_status` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `squid_request_status` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `http_username` (`http_username`),
  CONSTRAINT `access_log_ibfk_1` FOREIGN KEY (`http_username`) REFERENCES `users` (`username`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `host` varchar(128) NOT NULL,
  `date` int(40) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `iface` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `mac` (`mac`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `dns_settings`;
CREATE TABLE `dns_settings` (
  `key` varchar(32) NOT NULL,
  `value` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `guid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `queue`;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` int(40) NOT NULL,
  `end` int(40) NOT NULL,
  `status` int(1) NOT NULL,
  `error` text NOT NULL,
  `user` int(12) NOT NULL,
  `added` int(40) NOT NULL,
  `cmd` varchar(128) NOT NULL,
  `name` varchar(128) NOT NULL,
  `params` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `queue_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `hash` varchar(40) NOT NULL,
  `login` int(40) NOT NULL,
  `user_hash` tinytext,
  PRIMARY KEY (`id`),
  KEY `sessions_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `statistics`;
CREATE TABLE `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac_source` varchar(17) NOT NULL,
  `mac_target` varchar(17) NOT NULL,
  `income` int(64) NOT NULL,
  `outcome` int(64) NOT NULL,
  `date_hourly` int(10) NOT NULL,
  `type` enum('internal','external','','') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `mac` (`mac_source`),
  KEY `mac_target` (`mac_target`),
  CONSTRAINT `statistics_ibfk_1` FOREIGN KEY (`mac_source`) REFERENCES `clients` (`mac`),
  CONSTRAINT `statistics_ibfk_2` FOREIGN KEY (`mac_target`) REFERENCES `clients` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `stats`;
CREATE TABLE `stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) NOT NULL,
  `in` int(64) NOT NULL DEFAULT '0',
  `out` int(64) NOT NULL DEFAULT '0',
  `date` int(12) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mac` (`mac`),
  CONSTRAINT `stats_ibfk_1` FOREIGN KEY (`mac`) REFERENCES `clients` (`mac`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `home` tinytext,
  `passwd` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `hash` varchar(45) NOT NULL,
  `realname` text,
  `email` text,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `picture` text,
  `mobile` varchar(25) DEFAULT NULL,
  `last_login` int(45) DEFAULT NULL,
  `last_host` text,
  `last_ip` text,
  `enabled` int(1) NOT NULL DEFAULT '0',
  `shell` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_id` (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `wifi`;
CREATE TABLE `wifi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac` varchar(17) NOT NULL,
  `status_str` varchar(128) NOT NULL,
  `interface` varchar(12) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `changed` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `mac` (`mac`),
  CONSTRAINT `wifi_ibfk_1` FOREIGN KEY (`mac`) REFERENCES `clients` (`mac`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2017-05-06 14:56:15