<?php

if (isset($_GET["id"]) AND is_numeric($_GET["id"])) {
    $id = intval($_GET["id"]);
    $q = $db->query("SELECT * FROM users WHERE id = ".$id);
    
    $usr = $q->fetch_assoc();
    
    
    $f = new form2("useredit");
    $f->addInput("text", "username","Username:",$usr["username"])->addAttr("disabled",true);
    $home = $f->addInput("text", "home","Home:",$usr["home"]);
    //$f->addInput("text", "shell","Shell:",$usr["shell"]);
    $shell = $f->addInput("select", "shell","Shell:",array(
        "/bin/false"=>"False",
        "/bin/bash"=>"Bash",
        "/bin/sh"=>"sh",
        "/bin/ksh"=>"ksh"
    ))->addAttr("selected",$usr["shell"]);
    $password = $f->addInput("password", "password","Password:",$usr["password"]);
    $email = $f->addInput("email", "email","E-Mail:",$usr["email"]);
    $realname = $f->addInput("text", "realname","Real name:",$usr["realname"]);
    $first_name = $f->addInput("text", "firstname","First name:",$usr["first_name"]);
    $last_name = $f->addInput("text", "lastname","Last name:",$usr["last_name"]);
    $mobile = $f->addInput("text", "mobile","Mobile:",$usr["mobile"]);
    
    
    $f->addButton("Save", "Save");
    
    
    if ($f->validate()===true) {
        $db->query("UPDATE users SET
            email = '".$email->value()."',
            home = '".$home->value()."',
            shell = '".$shell->value()."',
            password = '".$password->value()."',
            realname = '".$realname->value()."',
            first_name = '".$first_name->value()."',
            last_name = '".$last_name->value()."',
            mobile = '".$mobile->value()."'                
            WHERE id = ".$id);
        if (!empty($db->error)) {
            $tpl->add("error",$db->error);
        }else{
            header("Location: ?p=user&id=".$id);
            exit;
        }
        
    }
    
 
    $tpl->add(array(
        "username"=>$usr["username"],
        "form"=>$f->show()
    ));
    $tpl->show("user_edit.tpl");    
}else{
    header("Location: notfound.html");
    exit;
}


