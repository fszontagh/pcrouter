#!/usr/bin/php
<?php
// THIS SCRIPT UPDATE THE CLIENT'S STATS FROM IPTABLES

$CONFIG = include '/etc/router/config.php';

$iptables = "/sbin/iptables -w";

$mysqli = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);

if ($mysqli->connect_errno) {
    log_error($mysqli->connect_error, 0);
    exit();
}



$query = $mysqli->query("SELECT * FROM clients WHERE deleted = '0' ORDER BY id DESC");
if (!empty($mysqli->error)) {
    log_error($mysqli->error, 0);
}
 $date = date("YmdH");
 echo $date."".PHP_EOL;

while ($row = $query->fetch_assoc()) {
   
    $ip = $row["ip"];
    $mac = $row["mac"];
    exec($iptables . " -C INET_IN -d " . $ip . " || " . $iptables . " -A INET_IN -d " . $ip);
    exec($iptables . " -C INET_OUT -s " . $ip . " || " . $iptables . " -A INET_OUT -s " . $ip);


    $in = intval(exec($iptables . " -v -n -x -L INET_IN | grep '" . $row["ip"] . "' | awk '{print $2}'"));
    $out = intval(exec($iptables . " -v -n -x -L INET_OUT | grep '" . $row["ip"] . "' | awk '{print $2}'"));

    $stat = $mysqli->query("SELECT * FROM stats WHERE mac = '" . $row["mac"] . "' AND date = '".$date."'");
    $stats = $stat->fetch_assoc();
    
    if (is_array($stats) AND !empty($stats) AND count($stats)>0) {

        if ($stats["in"] > $in OR $stats["out"] > $out) {
            $in = $stats["in"] + $in;
            $out = $stats["out"] + $out;
        }
            $mysqli->query("UPDATE `stats` SET `in` = ".$in.", `out` =  ".$out." WHERE mac = '".$mac."' AND date = '".$date."' ");
            echo "UPDATE: " . $row["mac"] . " (" . $row["ip"] . " " . $row["host"] . ") TO: " . $in . " - " . $out . "\n";
    }else{
        $mysqli->query("INSERT INTO stats (`in`,`out`,`mac`,`date`)VALUES(".$in.",".$out.",'".$mac."','".$date."')");
        if ($mysqli->error) {
            echo $mysqli->error.PHP_EOL;
        }
        echo "INSERT: " . $row["mac"] . " (" . $row["ip"] . " " . $row["host"] . ") TO: " . $in . " - " . $out . "\n";
    }

   // $mysqli->query("REPLACE INTO stats(`mac`,`in`,`out`, `date`)VALUES('" . $row["mac"] . "'," . $in . "," . $out . ",'".$date."')");
   // $mysqli->query("")
    
}

function log_error($str) {
    file_put_contents('php://stderr', $str . PHP_EOL, FILE_APPEND);
}
?>
