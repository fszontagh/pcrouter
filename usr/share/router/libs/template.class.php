<?php
class template {
    var $theme_path, $prefix, $error, $tpl_file, $last_error, $tpl;
    public $cache = true;
    private $cache_dir = 'tmp/cache/';
    private $compile_dir = 'tmp/compiled/';
    private $compiled = '';
    private $compiled_file;
    public $variables = array();
    public $size = 0;
    public $html_tidy = false;
    private $scope = array();
    private $scope_id = 0;
    private $in_scope = false;
    private $globals=array();
    function __construct($theme_path = '', $prefix = '') {
        if (!is_dir($this->cache_dir) AND $this->cache === true) {
            if (!@mkdir($this->cache_dir)) {
                $this->error[] = 'Nem sikerült  a cache mappa létrehozása: ' . $this->cache_dir;
            }
        }
        if (!is_dir($this->compile_dir)) {
            if (!@mkdir($this->compile_dir)) {
                $this->error[] = 'Nem sikerült a compile mappa létrehozása: ' . $this->compile_dir;
            }
        }
        if (!empty($theme_path)) {
            if (substr($theme_path, -1) != '/') {
                $theme_path .= '/';
            }
            $this->theme_path = $theme_path;
        }
        if (!empty($prefix)) {
            $this->prefix = $prefix;
        }
    }
    public function setGlobal($k,$v=null) {
        if (is_array($k)) {
            $na = array();
            foreach ($k as $kk => $kv) {
                $na["GLOBAL_".$kk] = $kv;
            }
            $this->globals = array_merge($this->globals,$na);
        }else{
            $this->globals["GLOBAL_".$k] = $v;
        }
        unset($k,$v);
    }
    public function tidy_html() {
        if (!class_exists("tidy")) {
            return;
        }
        $d      = ob_get_clean();
        $config = array(
            "break-before-br" => true,
            "indent" => true,
            "punctuation-wrap" => true,
            "sort-attributes" => false,
            "split" => false,
            "tab-size" => 5,
            "vertical-space" => false,
            "wrap" => 100,
            "wrap-attributes" => true,
            "wrap-jste" => true,
            "wrap-php" => false,
            "wrap-script-literals" => true,
            "wrap-sections" => false
        );
        $tidy   = new tidy();
        $tidy->parseString($d, $config);
        $tidy->cleanRepair();
        echo $tidy;
    }
    public function add($key, $val = null) {
        if (is_array($key)) {
            $this->variables = array_merge($this->variables, $key);
        } elseif ($val != null) {
            $this->variables[$key] = $val;
        }
        return $this->variables;
    }
    public function show($tpl_file, $instance_name = null) {
        $this->tpl_file = $this->theme_path . $tpl_file;
        if (!file_exists($this->tpl_file)) {
            exit("Not found: " . $this->tpl_file);
        }
        ob_start();
        include $this->tpl_file;
        $this->tpl = ob_get_clean();
        $this->tpl = preg_replace('/<!--(.*)-->/Uis', '', $this->tpl);
        $this->__parse($instance_name);
        extract($this->globals);
        extract($this->variables);
        $this->variables = array();
        include $this->compiled_file;
        $this->size += ob_get_length();
    }
    public function get($tpl_file, $instance_name = null) {
        $this->tpl_file = $this->theme_path . $tpl_file;
        if (!file_exists($this->tpl_file)) {
            exit("Not found: " . $this->tpl_file);
        }
        ob_start();
        include $this->tpl_file;
        $this->tpl = ob_get_clean();
        $this->tpl = preg_replace('/<!--(.*)-->/Uis', '', $this->tpl);
        $this->__parse($instance_name);
        extract($this->globals);        
        extract($this->variables);
        $this->variables = array();
        ob_start();
        include $this->compiled_file;
        return ob_get_clean();
    }
    private function __parse($instance = null) {
        if (file_exists($this->tpl_file)) {
            if ($instance != null) {
                $this->compiled = $this->__get_instance($instance);
                $this->compiled = preg_replace_callback('/\\{(.*?)\\}/', "template::__parse_statements", $this->compiled);
            } else {
                $this->compiled = preg_replace_callback('/\\{(.*?)\\}/', "template::__parse_statements", $this->__clear_output($this->tpl));
            }
            return $this->__compile($instance);
        } else {
            $this->error[] = 'Nem találom a template fájlt: ' . $this->tpl_file;
            return false;
        }
    }
    private function __get_instance($instance) {
        $tpl         = file($this->tpl_file);
        $is_instance = false;
        $ret         = '';
        $last_block  = '';
        foreach ($tpl as $line) {
            if (preg_match('/<!--' . $instance . '-->/', $line)) {
                $is_instance = true;
                $ret .= str_replace("<!--" . $instance . "-->", "", $line);
                continue;
            }
            if (preg_match('/<!--\/' . $instance . '-->/', $line) AND $is_instance === true) {
                $is_instance = false;
                $ret .= str_replace("<!--/" . $instance . "-->", "", $line);
                break;
            }
            if ($is_instance === true) {
                $ret .= $line;
            }
        }
        if (count($tpl) < 1) {
            echo "Instance nem található!";
            exit;
        } else {
            return $this->__clear_output($this->__removeInlineBlocks($ret));
        }
    }
    private function __removeInlineBlocks($line) {
        return preg_replace('/(<!--[A-Z].*?-->.*?<!--\/[A-Z].*?)-->/', '', $line);
    }
    private function __clear_output($o) {
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $o);
    }
    private function __parse_statements($m) {
        $return = "<?php ";
        if (substr($m[1], 0, 2) == "if") {
            $return .= preg_replace('/if\ (.*)/', 'if ($1):', $m[1]);
            if ($this->in_scope === true) {
                $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"] ", $return);
            }
        }
        if (substr($m[1], 0, 4) == "else") {
            $return .= preg_replace('/else/', 'else:', $m[1]);
        }
        if (substr($m[1], 0, 6) == "elseif") {
            $return .= preg_replace('/else\ (.*)/', 'elseif ($1):', $m[1]);
            if ($this->in_scope === true) {
                $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"]", $return);
            }
        }
        if ($m[1] == "/if") {
            $return .= "endif;";
        }
        if (substr($m[1], 0, 1) == "%") {
            $return .= "echo " . substr($m[1], 1) . ";";
        }
        if (substr($m[1], 0, 4) == 'loop') {
            if ($this->in_scope === true) {
                $this->second_scope = 0;
                $this->in_scope     = true;
                $old_scope_var      = "\$this->scope[(\$this->scope_id-" . ($this->second_scope + 1) . ")][\"" . substr($m[1], 6) . "\"]";
                $return .= "\$this->scope_id++; foreach(" . $old_scope_var . " as \$this->scope[\$this->setScope(" . $old_scope_var . ")]):";
            } else {
                $this->in_scope = true;
                $return .= "foreach(" . substr($m[1], 5) . " as \$this->scope[\$this->setScope(" . substr($m[1], 5) . ")]):";
            }
        }
        if ($m[1] == '/loop') {
            $this->in_scope = false;
            if (isset($this->second_scope) AND $this->second_scope > 0) {
                $return .= "endforeach; \$this->endScope((\$this->scope_id-" . $this->second_scope . "));";
            } else {
                if (isset($this->second_scope)) {
                    $this->second_scope++;
                }
                $return .= "endforeach; \$this->endScope(\$this->scope_id);";
            }
        }
        if (substr($m[1], 0, 1) == "$") {
            if (strstr($m[1],"GLOBAL_")!==false) {
                $var_name = substr($m[1],1);
                if (isset($this->globals[$var_name])) {
                    return $this->globals[$var_name];
                }
            } else {
                $return .= "echo " . $m[1] . "; ";
                if ($this->in_scope === true) {
                    $return = preg_replace('/\$([A-Za-z0-9\_]+)/', "\$this->scope[\$this->scope_id][\"$1\"]", $return);
                }
            }
        }
        return $return . " ?>";
    }
    private function __compile($instance = null) {
        if ($instance != null) {
            $this->compiled_file = $this->compile_dir . $this->theme_path . $instance . ".tpl";
        } else {
            $this->compiled_file = $this->compile_dir . $this->tpl_file;
        }
        if (!file_exists($this->compiled_file)) {
            $f__  = explode(DIRECTORY_SEPARATOR, $this->compiled_file);
            $file = end($f__);
            unset($f__);
            $path = explode(DIRECTORY_SEPARATOR, $this->compiled_file);
            unset($path[(count($path) - 1)]);
            $path = implode(DIRECTORY_SEPARATOR, $path);
            if (!is_dir($path)) {
                if (!@mkdir($path, 0755, true)) {
                    $this->error[] = 'Nem sikerült létrehozni a template könyvtárat: ' . $path;
                    return false;
                }
            }
        }
        file_put_contents($this->compiled_file, $this->compiled);
        return true;
    }
    private function setScope($data) {
        $this->in_scope = true;
        $this->scope++;
        $id                           = $this->scope_id;
        
        for ($i = 0;$i < count($data);$i++) {
            $data[$i] = array_merge($data[$i],$this->globals);
        }
        //echo "<pre>".print_r($data,true)."</pre>";
        $this->scope[$this->scope_id] = $data;
        return $id;
    }
    private function endScope($id) {
        $this->in_scope = false;
        if (isset($id)) {
            unset($this->scope[$id]);
        } else {
            unset($this->scope[$this->scope_id]);
        }
    }
    private function __cache() {
        return false;
    }
    public function lasterror() {
        if (count($this->error) > 0) {
            return end($this->error);
        }
    }
    function __destruct() {
        if (isset($this->tpl)) {
            unset($this->tpl);
        }
        unset($this->variables);
        unset($this->compiled);
    }
}
