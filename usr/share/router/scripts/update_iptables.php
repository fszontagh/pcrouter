#!/usr/bin/php
<?php

$old_ip_file = "/.old_ip";

exec("/sbin/ifconfig ppp0 | grep inet | awk '{print $2}' | awk -F: '{print $2}'",$external_ip);
$external_ip = trim($external_ip[0]);
$old_ip = exec("/sbin/iptables -L -n | grep REPLACETHIS | awk '{print $4}'");
//$old_ip = gethostbyname($old_ip);

if ($external_ip != $old_ip && !empty($external_ip)) {
	exec("/sbin/iptables-save -c > /tmp/iptables");
	$content = file_get_contents("/tmp/iptables");
	$new_content = str_replace($old_ip,$external_ip,$content);
	unlink("/tmp/iptables");
	file_put_contents("/tmp/tables_replaced",$new_content);
	echo "IP changed to ".$external_ip." from: ".$old_ip." ".date("Y-m-d H:i:s")."\n";
	exec("/sbin/iptables-restore -c < /tmp/tables_replaced && rm /tmp/tables_replaced");

#	$resolv = '# Dynamic resolv.conf(5) file for glibc resolver(3) generated by resolvconf(8)
#     DO NOT EDIT THIS FILE BY HAND -- YOUR CHANGES WILL BE OVERWRITTEN
#nameserver 127.0.0.1
#nameserver 8.8.4.4';

#	file_put_contents("/etc/resolv.conf",$resolv);

//	exec("/usr/sbin/service hostapd stop && killall -vw -s KILL hostapd && /etc/init.d/hostapd start");
//	exec("/usr/sbin/service dnsmasq restart");
}

if (empty($external_ip)) {
	echo "ERROR! ERROR! NO EXTERNAL IP!!";
}
?>
