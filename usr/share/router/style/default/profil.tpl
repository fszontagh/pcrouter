<div class="row">
    <div class="two-half column">
        <h4>Profil for {$name}</h4>
        <img style="float:left;width:96px;" width="96px" src="{$picture}">
        <p><em><a href="?p=profil_edit">Edit Your profile</a></em></p>
        <table class="u-full-width">  
            <tbody>
                <tr>
                    <th>Name</th>
                    <td>{$name}</td>
                </tr>
                <tr>
                    <th>First Name</th>
                    <td>{$first_name}</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td>{$last_name}</td>
                </tr>    
                <tr>
                    <th>Real Name</th>
                    <td>{$realname}</td>
                </tr>        
                <tr>
                    <th>Mobile</th>
                    <td>{$mobile}</td>
                </tr>        
                <tr>
                    <th>Email</th>
                    <td>{$email}</td>
                </tr>      
            </tbody>
        </table>

    </div>
</div>

