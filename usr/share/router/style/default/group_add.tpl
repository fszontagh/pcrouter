<div class="row">
    <div class="nine columns">
        <h2>Add system group</h2>
        {if isset($error)}<p class="error">{$error}</p>{/if}
        {$form}
    </div>

    <div class="three columns">
        <h5>Commands</h5>
        <ul>
            <li>
                <a href="?p=groups">Cancel</a>
            </li>
        </ul>
    </div>
</div>