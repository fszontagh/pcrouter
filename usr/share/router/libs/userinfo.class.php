<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of userinfo
 *
 * @author fszontagh
 */
class userinfo extends mysqli{
    
    public function getbyid($id) {
        
    }
    
   public function id() {
        return $this->user["id"];
    }

    public function username() {
        return $this->user["username"];
    }

    public function email() {
        return $this->user["email"];
    }
    public function realname() {
        return $this->user["realname"];
    }
    public function mobile() {
        return $this->user["mobile"];
    }

    public function first_name() {
        return $this->user["first_name"];
    }

    public function last_name() {
        return $this->user["last_name"];
    }

    public function uid() {
        return $this->user["uid"];
    }

    public function gid() {
        return $this->user["gid"];
    }

    public function home() {
        return $this->user["home"];
    }

    public function lastLogin() {
        return $this->user["last_login"];
    }
    public function picture() {
        return $this->user["picture"];
    }

    public function name() {
        if (is_null($this->user["first_name"]) AND is_null($this->user["last_name"])) {
            return $this->user["username"];
        }
        return $this->user["first_name"] . (empty($this->user["middle_name"]) ? "" : " " . $this->user["middle_name"]) . " " . $this->user["last_name"];
    }
    
}
