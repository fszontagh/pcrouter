<?php

header("Content-Type: text/html; charset=utf-8");
foreach (glob("libs/*.class.php") as $cls) {
    include_once $cls;
}
openlog('ROUTER WEBUI', LOG_NDELAY | LOG_PID | LOG_PERROR, LOG_SYSLOG);
$old_error_handler = set_error_handler("syserrorhandler");


$CONFIG = include '/etc/router/config.php';



$tpl = new template("style/default");
$user = new user($CONFIG["SYSTEM_KEY"], $CONFIG);


$tpl->add("title", $CONFIG["title"]);
$tpl->show("header.tpl");


$tpl->add("title", $CONFIG["title"]);
$tpl->add("user_authed", $user->is_authed);
$tpl->show("menu.tpl");


$db = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);
$queue = new queuemanager($db);
//$db->query("SET NAMES UTF8;");
//no user data, then login
if ($user->is_authed === false) {
    $form = new form2("login");
    $username = $form->addInput("text", "username", "Username");
    $password = $form->addInput("password", "password", "Password");
    $username->addAttr(array("autocomplete" => "off", "required" => true));
    $password->addAttr(array("autocomplete" => "off", "required" => true));
    $form->addButton("login", "Login");

    if ($form->validate() === true) {
        if ($user->authUser($username->value(), $password->value()) === false) {
            $tpl->add("error", $user->error);
        } else {
            header("Location: /");
            exit;
        }
    }

    $tpl->add("login_form", $form->show());
    $tpl->show("login.tpl");
} else {
    if (isset($_GET["p"]) AND ! empty($_GET["p"])) {
        $p = html_entity_decode($_GET["p"]);
        include 'progs/right_nav.php';
        if (file_exists("progs/" . $p . ".php")) {
            include 'progs/' . $p . ".php";
        } else {
            include 'progs/notfound.php';
        }
    } else {
        include 'progs/dashboard.php';
    }
    
}

$tpl->show("footer.tpl");

function syserrorhandler($errno, $errstr, $errfile, $errline) {
    if (!(error_reporting() & $errno)) {
        syslog(LOG_WARNING, $errfile . " (" . $errline . "): " . $errstr);
        return;
    }

    switch ($errno) {
        case E_USER_ERROR:
            syslog(LOG_ERR, $errfile . " (" . $errline . "): " . $errstr);
            break;

        case E_USER_WARNING:
            syslog(LOG_WARNING, $errfile . " (" . $errline . "): " . $errstr);
            break;

        case E_USER_NOTICE:
            syslog(LOG_NOTICE, $errfile . " (" . $errline . "): " . $errstr);
            break;

        default:
            syslog(LOG_WARNING, $errfile . " (" . $errline . "): " . $errstr);
            break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}

?>
