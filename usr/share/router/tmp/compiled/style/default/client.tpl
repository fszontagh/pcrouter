<div class="row">
    <div class="nine columns">
        <h2><?php echo $host;  ?></h2>
        <?php if (isset($form)): ?>
            <?php echo $form;  ?>
        <?php endif; ?>
        <table class="u-full-width">
            <thead>
            </thead>
            <tbody>
                <tr>
                    <th>Hostname: </th>
                    <td><?php echo $host;  ?></td>
                </tr>
                <tr>
                    <th>IP:</th>
                    <td><?php echo $ip;  ?></td>
                </tr>
                <tr>
                    <th>MAC:</th>
                    <td><?php echo $mac;  ?></td>
                </tr>
                <tr>
                    <th>Interface:</th>
                    <td><?php echo $iface;  ?></td>
                </tr>
            </tbody>
        </table>
<div>
    <p>
        <font color="#5cd65c">*Downloaded</font><br/>
        <font color="#ff704d">*Uploaded</font>
    </p>
  <canvas id="income_traffic" class="plot"></canvas>  
</div>
    </div>
    <div class="three columns">
        <h4>MODIFY</h4>
        <ul>
            <?php if ($fixed===false): ?>
                <li><a href="?p=client&id=<?php echo $id;  ?>&fixip=<?php echo $ip;  ?>">FIX this IP to this client</a></li>
                <?php endif; ?>
                <?php if ($fixed === true ): ?>
                <li><a class="red" href="?p=client&id=<?php echo $id;  ?>&remove_fixip=<?php echo $ip;  ?>">Remove fixed IP</a></li>
                <li><a href="?p=client&id=<?php echo $id;  ?>&change_ip=<?php echo $ip;  ?>">Change the IP</a></li>
                <?php endif; ?>
        </ul>
    </div>
</div>
<script>
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   var u = bytes / Math.pow(1024, i);
   u = u * 100;
   return Math.round(u, 2)/100 + ' ' + sizes[i];
};    
    var max = 0;
    var steps = 10;
    var chartData = {
    };
    function respondCanvas() {
        var c = $('#income_traffic');
        var ctx = c.get(0).getContext("2d");
        var container = c.parent();
        var $container = $(container);
        c.attr('width', $container.width()); //max width
        c.attr('height', "400px"); //max height                   
        //Call a function to redraw other content (texts, images etc)
        var chart = new Chart(ctx).Bar(chartData, {
            scaleOverride: true,
            scaleSteps: steps,
            scaleStepWidth: Math.ceil(max / steps),
            scaleStartValue: 0,
            scaleLabel: function (valuePayload) {
                var bytes = valuePayload.value;
                  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   var u = bytes / Math.pow(1024, i);
   u = u * 100;
   return Math.round(u, 2)/100 + ' ' + sizes[i];
            },
            // String - Template string for multiple tooltips
            multiTooltipTemplate: "<%= bytesToSize(value) %>",            
        });
    }
    var GetChartData = function () {
        $.ajax({
            url: "/api.cgi?mac=<?php echo $mac;  ?>",
            method: 'GET',
            dataType: 'json',
            success: function (d) {
                chartData = {
                    labels: d.DataLabels,
                    datasets: [
                        {
                            fillColor: "#5cd65c",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "#cacaca",
                            pointStrokeColor: "#fff",
                            data: d.DataSetsIn
                        },
   {
                            fillColor: "#ff704d",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "#cacaca",
                            pointStrokeColor: "#fff",
                            data: d.DataSetsOut
                        }                        
                    ]
                };
                max = Math.max.apply(Math, d.DataSetsIn);
                steps = 10;
                respondCanvas();
            }
        });
    };
    $(document).ready(function () {
    GetChartData();
        $(window).resize(setTimeout(respondCanvas, 500));
        setInterval("GetChartData",1000);
        GetChartData();
    });
</script>