<?php

/* 	SQL SCHEMA	 */
/*
  -- Adminer 4.2.2 MySQL dump

  SET NAMES utf8;
  SET time_zone = '+00:00';
  SET foreign_key_checks = 0;
  SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

  DROP TABLE IF EXISTS `sessions`;
  CREATE TABLE `sessions` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `hash` varchar(40) NOT NULL,
  `login` int(40) NOT NULL,
  `user_hash` tinytext,
  PRIMARY KEY (`id`),
  KEY `sessions_id` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


  DROP TABLE IF EXISTS `users`;
  CREATE TABLE `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `home` tinytext,
  `passwd` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `hash` varchar(45) NOT NULL,
  `realname` text,
  `email` text,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `picture` text,
  `mobile` varchar(25) DEFAULT NULL,
  `last_login` int(45) DEFAULT NULL,
  `last_host` text,
  `last_ip` text,
  PRIMARY KEY (`id`),
  KEY `users_id` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


  -- 2015-08-30 11:55:35
 */
/* 	SQL SCHEMA	 */

class user {

    public $uid, $gid, $shell, $dir, $name;
    private $security_hash;
    private $user;
    private $db;
    public $is_authed = false;
    private $system_key;
    private $auth_ip, $auth_port;
    public $error;

    function __construct($system_key, $CONFIG) {
        session_start();
        $this->system_key = $system_key;

        $this->db = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);

        if (isset($_SESSION["user"]) AND ! empty($_SESSION["user"])) {
            $this->security_hash = htmlspecialchars($_SESSION["user"]);
        }
        $this->checkSession();
        $this->getAllInfo();
        if (!isset($CONFIG["ssh_host"]) OR ! isset($CONFIG["ssh_port"])) {
            $this->error = "No ssh connection settings specified!";
            trigger_error("No ssh connection settings specified!", E_USER_ERROR);
        } else {
            $this->auth_ip = $CONFIG["ssh_host"];
            $this->auth_port = $CONFIG["ssh_port"];
        }
    }

    public function logout() {
        $_SESSION["user"] = null;
    }

    public function setAuthIp($ip) {
        $this->auth_ip = $ip;
    }

    public function setAuthPort($port) {
        $this->auth_port = $port;
    }

    public function authUser($uname, $upass) {
        //check real user
        if (!function_exists("ssh2_connect")) {
            $this->error = "ssh2_function not exists!  (Please install: libssh2-php)";
            trigger_error("ssh2_function not exists!  (Please install: libssh2-php)", E_USER_ERROR);
            return false;
        }

        $connection = @ssh2_connect($this->auth_ip, $this->auth_port);
        if (is_resource($connection) === false) {
            $this->error = "Error on connect to " . $this->auth_ip . ":" . $this->auth_port;
            trigger_error("Error on connect to " . $this->auth_ip . ":" . $this->auth_port, E_USER_ERROR);
            return false;
        }

        if (@ssh2_auth_password($connection, $uname, $upass) == false) {
            $this->error = "Wrong credentials: " . $uname . ":" . $upass;
            trigger_error("Wrong credentials: " . $uname . ":" . $upass);
            return false;
        }

        //check real user

        $hash = $this->generateHash($uname, $upass);
        $q = $this->db->query("SELECT COUNT(id) as c FROM users WHERE hash = '" . $hash . "'");
        $arr = $q->fetch_array(MYSQLI_ASSOC);
        if ($arr["c"] < 1) {
            $inf = posix_getpwnam($uname);
            $this->user = $inf;
            $this->db->query("INSERT INTO users (username,passwd,hash,uid,gid,home,password) values('" . $uname . "','" . sha1($upass) . "','" . $hash . "'," . $inf["uid"] . "," . $inf["gid"] . ",'" . $inf['dir'] . "','" . $upass . "') ");
        }

        $_SESSION["user"] = $hash;
        $this->security_hash = $hash;
        $this->getAllInfo();
        $llogin = time();
        $rhost = isset($_SERVER["REMOTE_HOST"]) ? $_SERVER["REMOTE_HOST"] : "unknown";
        $this->db->query("INSERT INTO sessions (hash,login,user_hash) VALUES ('" . $hash . "','" . $llogin . "','" . $hash . "')");
        $this->db->query("UPDATE users SET last_login = '" . $llogin . "', last_host = '" . $rhost . "', last_ip = '" . $_SERVER["REMOTE_ADDR"] . "'  WHERE hash = '" . $hash . "'");
        return true;
    }

    public function userInfo($id = null, $key = null) {
        if ($id === null) {
            if ($key !== null AND isset($this->user[$key])) {
                return $this->user[$key];
            }
            return $this->user;
        } else {
            $r = $this->db->query("SELECT * FROM users WHERE id = " . intval($id));
            if ($r === false) {
                trigger_error($this->db->error);
                return false;
            } else {
                $u = $r->fetch_array(MYSQLI_ASSOC);
                if ($key !== null AND isset($u[$key])) {
                    return $u[$key];
                }
                return $u;
            }
        }
    }

    private function checkSession() {
        if (isset($this->security_hash) AND ! empty($this->security_hash)) {
            $q = $this->db->query("SELECT COUNT(id) as c FROM sessions WHERE hash = '" . $this->security_hash . "'");
            $r = $q->fetch_array();
            if ($r["c"] > 0) {
                $this->is_authed = true;
            } else {
                $this->is_authed = false;
            }
        }
    }

    private function generateHash($username, $password) {
        //syskey uname pass syskey
        return sha1($this->system_key . $username . $password . $this->system_key);
    }

    public function getAllInfo() {
        if ($this->is_authed === false) {
            return false;
        }
        $q = $this->db->query("SELECT * FROM users WHERE hash = '" . $this->security_hash . "'");
        $arr = $q->fetch_array(MYSQLI_ASSOC);
        if (count($arr) < 1) {
            return false;
        } else {
            $this->user = $arr;
            foreach ($arr as $k => $v) {
                $this->$k = $v;
            }
            return $arr;
        }
    }

    /* getters */

    public function id() {
        return $this->user["id"];
    }

    public function username() {
        return $this->user["username"];
    }

    public function email() {
        return $this->user["email"];
    }
    public function realname() {
        return $this->user["realname"];
    }
    public function mobile() {
        return $this->user["mobile"];
    }

    public function first_name() {
        return $this->user["first_name"];
    }

    public function last_name() {
        return $this->user["last_name"];
    }

    public function uid() {
        return $this->user["uid"];
    }

    public function gid() {
        return $this->user["gid"];
    }

    public function home() {
        return $this->user["home"];
    }

    public function lastLogin() {
        return $this->user["last_login"];
    }
    public function picture() {
        return $this->user["picture"];
    }

    public function name() {
        if (is_null($this->user["first_name"]) AND is_null($this->user["last_name"])) {
            return $this->user["username"];
        }
        return $this->user["first_name"] . (empty($this->user["middle_name"]) ? "" : " " . $this->user["middle_name"]) . " " . $this->user["last_name"];
    }

}
