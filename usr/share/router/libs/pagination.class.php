<?php

class page {

    private $url;

    function __construct() {
        $this->url = $_SERVER["QUERY_STRING"];
    }

    function url($method, $url, $v) {
        if ($method == "add") {
            return $this->url.="&display=" . $v;
        }
    }

    function url_origin($s, $use_forwarded_host = false) {
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on' );
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . ( ( $ssl ) ? 's' : '' );
        $port = $s['SERVER_PORT'];
        $port = ( (!$ssl && $port == '80' ) || ( $ssl && $port == '443' ) ) ? '' : ':' . $port;
        $host = ( $use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST']) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null );
        $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    function full_url($s, $use_forwarded_host = false) {
        return $this->url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
    }

}

class pagination {

    private $url = array();
    private $num_pages = 1;
    private $start = 0;
    private $display;
    private $start_display;
    public $page;

    function __construct($num_records, $display = 10) {


        $this->display = $display;
        if (isset($_GET['display']) && is_numeric($_GET['display'])) {
            $this->display = (int) $_GET['display'];
        }


        if ($num_records > $this->display) {
            $this->num_pages = ceil($num_records / $this->display);
        }
        $this->start_display = " LIMIT {$this->start}, {$this->display}";
    }

    public function url($int) {
        parse_str($_SERVER["QUERY_STRING"], $this->url);
        $this->url["display"] = $int;
        $this->url = http_build_query($this->url);
        return "?" . $this->url;
    }

    public function display($split = 5) {
        $html = '';
        if ($this->num_pages <= 1) {
            return $html;
        }

        $current_page = ($this->start / $this->display) + 1;
        $begin = $current_page - $split;
        $end = $current_page + $split;
        if ($begin < 1) {
            $begin = 1;
            $end = $split * 2;
        }
        if ($end > $this->num_pages) {
            $end = $this->num_pages;
            $begin = $end - ($split * 2);
            $begin++; // add one so that we get double the split at the end
            if ($begin < 1)
                $begin = 1;
        }
        if ($current_page != 1) {
            $html .= '<a class="first" title="First" href="' . $this->url(0) . '">&laquo;</a>';
            $html .= '<a class="prev" title="Previous" href="' . $this->url($this->start - $this->display) . '">Previous</a>';
        } else {
            $html .= '<span class="disabled first" title="First">&laquo;</span>';
            $html .= '<span class="disabled prev" title="Previous">Previous</span>';
        }
        for ($i = $begin; $i <= $end; $i++) {
            if ($i != $current_page) {
                $html .= '<a title="' . $i . '" href="' . $this->url($this->display * ($i)) . '">' . $i . '</a>';
            } else {
                $html .= '<span class="current">' . $i . '</span>';
            }
        }
        if ($current_page != $this->num_pages) {
            $html .= '<a class="next" title="Next" href="' . $this->url($this->start + $this->display) . '">Next</a>';
            $last = ($this->num_pages * $this->display) - $this->display;
            $html .= '<a class="last" title="Last" href="' . $this->url($last) . '">&raquo;</a>';
        } else {
            $html .= '<span class="disabled next" title="Next">Next</span>';
            $html .= '<span class="disabled last" title="Last">&raquo;</span>';
        }
        return '<div class="pagination">' . $html . '</div>';
    }

    public function limit() {
        return $this->start_display;
    }

}

?>