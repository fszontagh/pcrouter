<div class="row">
    <div class="nine columns">
        <h2>Userinfo</h2>

        {if isset($form)}
            {$form}
        {/if}
        <table class="u-full-width">
            <thead>

            </thead>
            <tbody>
                <tr>
                    <th>Username:</th>
                    <td>{$username}@{$host}{if $enabled==0}<br/><small><em class="red">Login not allowed</em></small>{/if}</td>
                </tr>
                <tr>
                    <th>IP:</th>
                    <td>{$last_ip}</td>
                </tr>
                <tr>
                    <th>Last Login:</th>
                    <td>{$last_login}</td>
                </tr>
                <tr>
                    <th>Interface:</th>
                    <td>{$iface}</td>
                </tr>
                <tr>
                    <th>Shell:</th>
                    <td>{$shell}</td>
                </tr>
                <tr>
                    <th>Home:</th>
                    <td>{$home}</td>
                </tr>
                <tr>
                    <th>Groups:</th>
                    <td>{$groups}</td>
                </tr>

            </tbody>
        </table>
    </div>

    <div class="three columns">
        <h5>OPTIONS</h5>
        <ul>
            {if $enabled==1}
            <li>
                <a href="?p=user&id={$id}&enabled=0">Disable login</a>
            </li>
            {else}
            <li>
                <a href="?p=user&id={$id}&enabled=1">Enable login</a>
            </li>
            {/if}
            <li>
                <a href="?p=user_edit&id={$id}">Edit</a>
            </li>
        </ul>
    </div>
</div>