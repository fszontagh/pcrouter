<?php

/*
 * This cronjob must run in all minutes
 * * * * * * /usr/share/router/scripts/cronjob.php
 */
ini_set("include_path", "/usr/share/router/libs");
include 'queuemanager.class.php';
$CONFIG = include '/etc/router/config.php';

$mysqli = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);


if ($mysqli->connect_errno) {
    log_error($mysqli->connect_error, 0);
    exit();
}
$queue = new queuemanager($mysqli);

$list = $queue->jlist(0);
if (count($list) < 1) {
    $mysqli->close();
    exit;
}
foreach ($list as $idx => $job) {
    echo "Name: " . $job->name . " id: " . $job->id . " cmd: " . $job->cmd . PHP_EOL;

    //remove group
    if ($job->name == "group_del") {
        $queue->start($job->id);

        $handle = popen($job->cmd . ' 2>&1', 'r');
        $read = fread($handle, 2096);
        pclose($handle);

        if (!empty($read)) {
            trigger_error($read);
            $queue->error($job->id, $read);
            $queue->finish($job->id);
            continue;
        }
        $params = array();
        if (isset($job->params)) {
            $params = json_decode($job->params);
        }
        if (!isset($params->group_name)) {
            $queue->error($job->id, "No params found!");
            continue;
        }
        $mysqli->query("DELETE FROM `groups` WHERE id = " . $params->id);
        $queue->finish($job->id);
    }

    //goup add
    if ($job->name == "group_add") {
        $queue->start($job->id);

        $handle = popen($job->cmd . ' 2>&1', 'r');
        $read = fread($handle, 2096);
        pclose($handle);

        if (!empty($read)) {
            trigger_error($read);
            $queue->error($job->id, $read);
            $queue->finish($job->id);
            continue;
        }

        $params = array();
        if (isset($job->params)) {
            $params = json_decode($job->params);
        }
        if (!isset($params->group_name)) {
            $queue->error($job->id, "No params found!");
            continue;
        }
        $id = intval(exec("cat /etc/group | grep \"" . $params->group_name . "\:\" | awk -F: '{print $3}'"));
        $mysqli->query("INSERT INTO groups (name,guid)VALUES('" . $params->group_name . "'," . $id . ")");
        syslog(LOG_INFO, "Added new group: " . $params->group_name . " by: " . $job->user);

        $queue->finish($job->id);
    }
}

$mysqli->close();

function log_error($str) {
    file_put_contents('php://stderr', $str . PHP_EOL, FILE_APPEND);
}
