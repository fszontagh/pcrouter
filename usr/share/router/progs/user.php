<?php

if (isset($_GET["id"]) AND is_numeric($_GET["id"])) {
    $id = intval($_GET["id"]);


    if (isset($_GET["enabled"]) AND is_numeric($_GET["enabled"])) {
        $enabled = intval($_GET["enabled"]) === 1 ? 1 : 0;
        $db->query("UPDATE users SET enabled = " . $enabled . " WHERE id = " . $id);
        header("Location: ?p=user&id=" . $id);
        exit;
    }

    $quser = $db->query("SELECT users.*, clients.host, clients.id as cid, clients.iface FROM users, clients WHERE users.id = " . $id . " AND clients.ip = users.last_ip");
    $user = $quser->fetch_assoc();

    $user["last_login"] = date("Y-m-d H:i:s", $user["last_login"]);

    $groups = exec("/usr/bin/groups ".$user["username"]." | awk -F: '{print $2}'");
    
    $g = explode(" ",$groups);
    $gr = array();
    foreach ($g as $k=>$v) {
        if (!empty(trim($v))) {
            $gr[] = $v;
        }
    }
    
    $tpl->add("groups",  implode(", ",$gr));
    $tpl->add($user);
    $tpl->show("user.tpl");
}

