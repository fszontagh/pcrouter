<div class="row">    
    <div class="sixteen columns">
        <div class="four columns offset-by-four">
            <h4 class="red">ERROR</h4>
            <p>
                The requested page not found!<br/>
                <a href="#" onclick="javascript:history.back();return false;">Back...</a>
            </p>
        </div>
    </div>
</div>

