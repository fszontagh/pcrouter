<?php

$q = $db->query("SELECT `key`,`value` FROM dns_settings ORDER BY `key` DESC");
$settings = array();
while ($setting = $q->fetch_array()) {
    $settings[$setting["key"]] = $setting["val"];
    
}
$tpl->add("settings",$settings);
$tpl->show("dns_settings.tpl");