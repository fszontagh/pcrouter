<?php

$form = new form2(null, "POST", true, true);


$avatar = $form->addInput("file", "avatar","Avatar");
$first_name = $form->addInput("text", "first_name", "First name", $user->first_name);
$last_name = $form->addInput("text", "last_name", "Last name", $user->last_name);
$realname = $form->addInput("text", "real_name", "Real name", $user->realname);
$mobile = $form->addInput("text", "mobile", "Mobile", $user->mobile);
$email = $form->addInput("email", "email", "EMail", $user->email);
$form->addButton("save", "Save")->addAttr("class", "button-primary");



$first_name->addAttr(array("autocomplete"=>"off","required"=>true));
$last_name->addAttr(array("autocomplete"=>"off","required"=>true));
$realname->addAttr(array("autocomplete"=>"off","required"=>false));



if ($form->validate()===true) {
    $avatar = $user->picture();
    $target_file = "avatars". DIRECTORY_SEPARATOR . basename($_FILES["avatar"]["name"]);
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $check = getimagesize($_FILES["avatar"]["tmp_name"]);
    
    if($check === false) {
        $tpl->add("error","This is not an image file!");        
    }else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {
                $avatar = $target_file;
            }
    }    
    
    
    $db->query("UPDATE users SET picture = '".$avatar."', "
            . "first_name = '".$first_name->value()."',"
            . "last_name = '".$last_name->value()."',"
            . "realname = '".$realname->value()."',"
            . "mobile= '".$mobile->value()."',"
            . "email = '".$email->value()."'"
            . " WHERE id = ".$user->id());
    if (!empty($db->error)) {
        $tpl->add("error",$db->error);
    }else{
        header("Location: ?p=profil");
        exit;
    }
}


$tpl->add("name",$user->name());
$tpl->add("form", $form->show());
$tpl->show("profil_edit.tpl");
