#!/usr/bin/php
<?php

$CONFIG = include '/etc/router/config.php';

$iptables = "/sbin/iptables -w";

$mysqli = new mysqli($CONFIG["mysql_host"], $CONFIG["mysql_user"], $CONFIG["mysql_pass"], $CONFIG["mysql_db"]);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
if (!isset($argv[1]) OR empty($argv[1])) {
	exit;
}
$cmd	= trim($argv[1]);
$mac	= trim($argv[2]);
$ip	= $argv[3];
$host	= isset($argv[4])?$argv[4]:"";

$iface = getenv("DNSMASQ_INTERFACE");//DNSMASQ_INTERFACE


#file_put_contents("/root/dnsmasq_script.log","CMD: ".$cmd." ip: ".$ip." mac: ".$mac." host: ".$host." iface: ".$iface.PHP_EOL,FILE_APPEND);
#file_put_contents("/root/dnsmasq.log",print_r($argv,true).PHP_EOL,FILE_APPEND);
if ($cmd == 'add' OR $cmd == 'old') {

	if ($cmd=="add") {
		//check if exists in the clients list
		$check = $mysqli->query("SELECT COUNT(id) as chk FROM clients WHERE mac = '".$mac."'");
		$c = $check->fetch_assoc();
		
		if ($c["chk"]>0) {
			$mysqli->query("UPDATE clients SET ip = '".$ip."', host = '".$host."', date = ".time().", deleted = 0 WHERE mac = '".$mac."'");
		}else{
			$mysqli->query("INSERT INTO clients (mac,ip,host,date,iface,deleted)VALUES('".$mac."','".$ip."','".$host."',".time().",'".$iface."','0')");
		}
		
		if (!empty($mysqli->error)) {
			file_put_contents("/root/dnsmasq_script.log","MYSQL ERROR: ".$mysqli->error.PHP_EOL,FILE_APPEND);
		}
		//add new rules for the new IP address
		exec($iptables." -C INET_IN -d ".$ip." || ".$iptables." -A INET_IN -d ".$ip);
		exec($iptables." -C INET_OUT -s ".$ip." || ".$iptables." -A INET_OUT -s ".$ip);
	}
	if ($cmd == "old") {
		//$mysqli->query("UPDATE clients SET ip = '".$ip."', host = '".$host."', date = ".time().", deleted = 0 WHERE mac = '".$mac."'");
		$mysqli->query("REPLACE INTO clients (ip,host,date,deleted,mac)VALUES('".$ip."','".$host."','".time()."',0,'".$mac."')");
		exec($iptables." -C INET_IN -d ".$ip." || ".$iptables." -A INET_IN -d ".$ip);
                exec($iptables." -C INET_OUT -s ".$ip." || ".$iptables." -A INET_OUT -s ".$ip);		
	}
	//windows phone
	if ($mac == "3c:83:75:e2:f7:cc") {
		syslog(LOG_INFO,"Waking up ghost001");
		exec("/usr/bin/wakeonlan bc:5f:f4:f7:a5:3a");
	}

}

if ($cmd == 'del') {
	$mysqli->query("UPDATE clients SET deleted = '1' WHERE mac = '".$mac."'");

	//delete unused rules from the chains
        exec($iptables." -C INET_IN -d ".$ip." && ".$iptables." -D INET_IN -d ".$ip);
        exec($iptables." -C INET_OUT -s ".$ip." && ".$iptables." -D INET_OUT -s ".$ip);

}
?>
