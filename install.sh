#!/bin/bash

echo -en "This script will install the router interface...\n"

apt-get update >/dev/null
apt-get upgrade -y 
apt-get install php5-cgi php5-cli php5-mysql libssh2-php dnsmasq mysql-server git -y
#syslog for lxc container
apt-get install -y syslogd

apt-get install gcc make -y
useradd --system -M -d /var/www www 
cd /tmp
if [ ! -e "/usr/bin/thttpd" ]; then
	wget http://www.acme.com/software/thttpd/thttpd-2.27.tar.gz
	tar xfv thttpd-2.27.tar.gz
	cd thttpd-2.27
	./configure --prefix=/usr
	make -j2
	mkdir /usr/man/man1 -p
	make install
	cd /tmp
	rm -r /tmp/thttpd-2.27*	
fi;
if [ ! -d "pcrouter" ]; then
	git clone https://bitbucket.org/fszontagh/pcrouter.git
fi;
cd /tmp/pcrouter

if [ ! -e "/etc/router/config.php" ]; then
        cp -rfvp etc/* /etc/
        cp -rfvp usr/share/* /usr/share/
        cp -prfv var/www/router/* /var/www/
        mysqladmin -u root -p create dns;
		mysql -u root -p dns < /usr/share/router/dns_nodata.sql
		crontab -l > /tmp/installcron
		echo "* * * * * /usr/bin/php /usr/share/router/scripts/cronjob.php " >> /tmp/installcron
		crontab /tmp/installcron
		rm /tmp/installcron
		rm -r /tmp/pcrouter
fi;


echo -en "Please change the config file: /etc/router/config.php to the installed MYSQL server... \n"
