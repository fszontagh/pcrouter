<?php

/**
 * Manage system queue to webui jobs
 *
 * @author fszontagh
 */
class queuemanager {

    /** @var resource */
    private $sqli;

    /** @var array */
    private $jobs;

    function __construct($db) {
        if (get_class($db) != 'mysqli') {
            trigger_error("The param1 is not mysql resource! (" . get_class($db) . ")", E_USER_ERROR);
            return;
        }
        $this->sqli = $db;
    }

    /**
     * List jobs in the queue
     *
     * @param enum $status The status of the job, 0 - not started, 1 - started, 2 - finished
     * @param string $order Order by the return
     * @param string $asc Ordering
     * @return array The return list of jobs in array
     */
    public function jlist($status, $order = 'added', $asc = 'asc') {
//        trigger_error("got list request");
        $q = $this->sqli->query("SELECT * from queue WHERE status = '" . intval($status) . "' ORDER BY " . $order . " " . $asc);
        if ($q === false) {
            trigger_error($this->sqli->error, E_ERROR);
            exit;
        }
        while ($job = $q->fetch_assoc()) {
            $this->jobs[] = new queuejob($job);
        }
        //   print_r($this->jobs);
        return $this->jobs;
    }

    /**
     * Erease a job from the queue
     * @param int $id The id of the job
     * @return bool Return when deleted from mysql, else return false
     */
    public function del($id) {
        if (isset($this->jobs[$id])) {
            $r = $this->sqli->query("delete from `queue` where id = " . intval($id));
            if ($r === true) {
                unset($this->jobs[$id]);
            }
            return $r;
        }
    }

    /**
     * Finish a job in the queue
     * @param id $id
     * @return bool Return true if finished, or false when not
     */
    public function finish($id) {
        trigger_error("finish called for id: ".$id);
        $t = time();
        $s = 2;
        $ss = $this->sqli->query("UPDATE `queue` SET `status` = '" . $s . "', `end` = " . $t . " WHERE id = " . $id);
        if ($ss === true) {
            $this->jobs["status"] = $s;
            $this->jobs["end"] = $t;
        }else{
            trigger_error($this->sqli->error,E_ERROR);
            exit;
        }
        
        return $ss;
    }

    /**
     *
     * @param int $id The job id
     */
    public function start($id) {
        $this->jobs["status"] = 1;
        $this->jobs["start"] = time();
        $this->sqli->query("UPDATE `queue` SET `status` = '1', `start` = " . time() . " WHERE id = " . $id);
    }

    /**
     * Set the job finished, and set the error string
     * @param id $id The job id
     * @param string $str The error string
     */
    public function error($id, $str) {
        $str = $this->sqli->real_escape_string($str);
        if ($this->sqli->query("update `queue` set `error` = '" . $str . "' WHERE id = " . $id)===false) {
            trigger_error($this->sqli->error);
            exit;
        }
     //   $this->finish($id);
    }

    /**
     * Add a new job to the queue
     * @param string $name The name of the job
     * @param id $user The user id, who added the job
     * @param string $cmd The raw command to the shell
     * @param string $params Add extra params to the job processer in json
     * @return mixed Return false, is something wrong, else return the new job object @link queuejob
     */
    public function add($name, $user, $cmd, $params='') {
        if (is_array($params)) {
            $params = json_encode($params);
        }
        //$cmd = mysqli::real_escape_string($cmd);
        $cmd = $this->sqli->real_escape_string($cmd);
        $added = time();
        $query = "INSERT INTO `queue` (`name`,`user`,`cmd`,`added`,`status`,`params`)";
        $query .= " VALUES ";
        $query .= " ('" . $name . "'," . intval($user) . ",'" . $cmd . "'," . $added . ",'0','".$params."') ";
        $s = $this->sqli->query($query);


        if ($s === false) {
            trigger_error($this->sqli->error . "\nQuery: " . $query, E_USER_ERROR);
            return false;
        }
        $id = $this->sqli->insert_id;
        $this->jobs[$id] = new queuejob(array(
            "status" => 0,
            "name" => $name,
            "user" => $user,
            "cmd" => $cmd,
            "added" => $added
        ));
        return $this->jobs[$id];
    }

    /**
     * Get the job
     * @param id $id The job id
     * @return object Return with the @link queuejob object
     */
    public function get($id) {
        return $this->jobs[$id];
    }

}

/**
 * Handle one job in the queue manager
 *
 * @property string $name The job name
 * @property string $cmd The command to run as job
 * @property string $error Error string, if the job failed to finish properly
 * @property string $params Add extra params to the job
 * @property int $start The job's start time in unix timestamp
 * @property int $end The job's finish time in unix timestamp
 * @property enum(0,1,2) $status The job's status 0 - not started, 1 - started, 2 - finished
 * @property int $id The mysql id of the job
 * @property int $user The user id from the users table
 * @property int $added The unix time stamp, when the job added
 */
class queuejob {
    /*     * #@+ @var string */

    public $name;
    public $cmd;
    public $error;
    public $params;
    /*     * #@+ @var int */
    public $start, $end, $status, $id, $user, $added;

    function __construct($job_array) {
        if (is_array($job_array)) {
            foreach ($job_array as $k => $v) {
                $this->$k = $v;
            }
        }
    }

}
